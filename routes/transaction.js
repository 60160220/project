const express = require("express");
const router = express.Router();
const transactionController = require('../controller/TransactionController'); 
module.exports = router;

router.get('/:user', transactionController.getTransaction)
router.get('/amount/:user', transactionController.getAmount)
router.post('/deposit', transactionController.addDeposit)
router.post('/withdraw', transactionController.addWithdraw)
router.put('/:id', transactionController.putTransaction)
router.delete('/:id', transactionController.deleteTransaction)
// router.post('/transfer', transactionController.addTransfer)