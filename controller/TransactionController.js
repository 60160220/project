const db = require('mongodb');
const Transaction = require('../models/transaction');
async function Amount(req_user) {
  const customerMoney = await Transaction.aggregate([
    {
      $match: { user: req_user }
    },
    {
      $group: { _id: '$user', sum: { $sum: '$money' } }
    },
    {
      $project: { _id: 1, sum: 1 }
    }
  ]);
  if(customerMoney[0] != null){
    return customerMoney[0].sum;
  }
  else{
    return 0;
  }
}

const transactionController = {
  async getTransaction(req, res) {
    const amount = await Amount(req.params.user);
    const transaction = await Transaction.find({ user: req.params.user }).sort({datetime: 'desc'});
    res.json({ transactions: transaction, sum: amount });
  },
  async addDeposit(req, res) {
    // ฝากเงิน
    const deposit = new Transaction({
      user: req.body.user,
      description: 'ฝากเงิน',
      money: req.body.money,
      datetime: Date.now()
    });
    await deposit.save();
    res.json({ status: true });
  },
  async addWithdraw(req, res) {
    const amount = await Amount(req.body.user);
    if (amount >= req.body.money) {
      const withdraw = new Transaction({
        user: req.body.user,
        description: 'ถอนเงิน',
        money: 0 - req.body.money,
        datetime: Date.now()
      });
      await withdraw.save();
      res.json({ status: true });
    } else {
      res.json({ status: false, message: 'จำนวนเงินไม่เพียงพอ' });
    }
  },
  async getAmount(req, res) {
    const dataAmount = await Amount(req.params.user);
    res.json({ status: true, amount: dataAmount });
  },
  async putTransaction(req,res){
    if(req.body.description === 'ถอนเงิน'){
      req.body.money = 0-req.body.money
    }  
    const payload = req.body
    console.log(payload)
    await Transaction.updateOne({_id:req.params.id}, payload).then(res.json({status: true})).catch((err)=>{console.log(err)})
  },
  async deleteTransaction(req,res){
    const id = req.params.id
    await Transaction.deleteOne({_id:id}).then(res.json({status: true})).catch((err)=>{console.log(err)})
  }

  // {}
};
module.exports = transactionController;
