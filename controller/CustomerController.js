const Customer = require('../models/customer');

const customerController = {
  async getCustomer(req, res) {
    const customer = await Customer.find({});
    res.json(customer);
  },
  async getCustomerByUser(req, res) {
    const customer = await Customer.findOne({
      user: req.params.user
    });
    if (customer == null) {
      res.json({ status: false, message: 'ไม่พบบัญชีผู้ใช้หรือรหัสผ่านนี้'});

    } else {
      res.json({ status: true, message: '', customer: customer });
    }
  },
  async getCustomerLogin(req, res, next) {
    const customer = await Customer.findOne({
      user: req.body.user
    });

    if (customer == null) {
      res.json({ status: false, message: 'ไม่พบบัญชีผู้ใช้หรือรหัสผ่านนี้' });
    } else if (customer.password === req.body.password) {
      res.json({ status: true, message: '', customer: customer });
    } else {
      res.json({ status: false, message: 'ไม่พบบัญชีผู้ใช้หรือรหัสผ่านนี้' });
    }
  },
  async addCustomer(req, res) {
    const payload = req.body;
    const customer = new Customer(payload);
    await customer.save().catch((err)=>{
      if(err.code === 11000){ //
        res.json({ status: false,message: 'ชื่อผู้ใช้นี้ไม่สามารถใช้ได้ เพราะได้ถูกใช้ไปแล้ว' });
      } else {
        res.json({ status: false,message: err.errmsg });
      }
    });
    res.json({ status: true });
  }
};

module.exports = customerController;
