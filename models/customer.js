const mongoose = require('mongoose')
const Schema = mongoose.Schema
const customerSchema = new Schema({
  user: {
    type: String,
    unique: true
  },
  password: String,
  firstname:String,
  lastname:String
})

const CustomerModel = mongoose.model('customer', customerSchema,'customer')

module.exports = CustomerModel