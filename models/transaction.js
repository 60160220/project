const mongoose = require('mongoose')
const Schema = mongoose.Schema
const transactionSchema = new Schema({
  user: String,
  description : String,
  money:Number,
  datetime:Date,
  userReference: String 
})

const TransactionModel = mongoose.model('transaction', transactionSchema,'transaction')

module.exports = TransactionModel